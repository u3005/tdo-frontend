import "./App.css";
import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
// import PrivateRoute from "./components/PrivateRoute";
import CompNavbar from "./components/Navigator";
import CompLanding from "./components/Landing";
import CompHome from "./components/Home";
import CompCitasListar from "./components/CitasListar";
import CompCitasCrear from "./components/CitasCrear";
import CompCitasEditar from "./components/CitasEditar";

function App() {
	return (
		<Router>
			<CompNavbar />
			<Routes>
				<Route path="/" element={<CompLanding />} />
				{/* <Route path="/home" element={<PrivateRoute />}> */}
				<Route path="/home" element={<CompHome />} />
				<Route path="/home/list" element={<CompCitasListar />} />
				<Route path="/home/create" element={<CompCitasCrear />} />
				<Route path="/home/edit" element={<CompCitasEditar />} />
				{/* </Route> */}
			</Routes>
		</Router>
	);
}

export default App;
