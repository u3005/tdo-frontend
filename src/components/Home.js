import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useNavigate } from "react-router-dom";

const CompHome = () => {
	const navigate = useNavigate();

	return (
		<>
			<Container style={{ width: "25rem" }}>
				<Col className="mt-5">
					<Card className="text-center">
						<Card.Header>SELECCIONE LA OPCIÓN QUE DESEA REALIZAR</Card.Header>{" "}
						<Card.Body>
							<Row>
								<Col>
									<Button variant="dark" onClick={() => navigate("/home/create")}>
										AGENDAR SERVICIO
									</Button>
								</Col>
								<Col>
									<Button variant="dark" onClick={() => navigate("/home/list")}>
										VER MIS SERVICIOS
									</Button>
								</Col>
							</Row>
						</Card.Body>
					</Card>
				</Col>
			</Container>
		</>
	);
};

export default CompHome;
