import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import { GetFromDB } from "./GetDB";

const URI = require("./URI");

const CompCitasListar = () => {
	const [modalState, setModalState] = useState("close");
	const handleShowModalEliminar = () => {
		setModalState("modal-eliminar");
	};
	const handleClose = () => {
		setModalState("close");
	};

	const navigate = useNavigate();

	var [citas, setCitas] = useState([]);
	var [cita, setCita] = useState("");
	var [medicos, setMedicos] = useState([]);
	var [medico, setMedico] = useState("");
	var [_id_paciente, set_IdPaciente] = useState("");

	useEffect(() => {
		_id_paciente = JSON.parse(localStorage.getItem("_id_paciente"));
		if (_id_paciente) set_IdPaciente(_id_paciente);
	}, []);

	const getMedicos = async () => {
		const res = await axios.get(`${URI}getMedicos/`);
		const medicosArr = [];
		for (var i in res.data) {
			medicosArr.push(i, res.data[i]);
		}
		setMedicos(medicosArr[1]);
	};

	useEffect(() => {
		getMedicos();
	}, []);

	////////////////////////////////////

	useEffect(() => {
		GetFromDB("Medicos").then((res) => {
			setMedicos(res);
		});
		GetFromDB("CitasPaciente").then((res) => {
			setCitas(res);
		});
	}, []);

	// const arrPaths = ["Medicos", "CitasPaciente"];
	// // var arrDB = GetFromDB(arrPaths);
	// // console.log("arrDB", arrDB);
	// console.log("medicos", medicos);

	// medicos = [];

	// setMedicos(medicos);
	// citas = [];

	// for (var i in arrDB) {
	// 	// arr.push(i, res.data[i]);
	// 	console.log("DB i: ", arrDB[i]);
	// }

	// console.log("medicos", arrDB.get("aAlgp"));
	// console.log("citas", arrDB.get("CitasPaciente"));
	// console.log("citas", citas);

	// setMedicos(arrDB[0]);
	// setCitas(arrDB[1]);

	// console.log("arrDB", arrDB);

	// const citas = GetFromDB("CitasPaciente")
	// const medicos = GetFromDB("Medicos");

	// const citas = GetFromDB("CitasPaciente");
	// console.log("CITAS: ", citas);
	// const medicos = GetFromDB("Medicos");
	// console.log("MEDICOS: ", medicos);

	//////////////////////////////////////////////

	// const getCitasPaciente = async () => {
	// 	const res = await axios.get(`${URI}getCitasPaciente/${_id_paciente}`);
	// 	const citassArr = [];
	// 	for (var i in res.data) {
	// 		citassArr.push(i, res.data[i]);
	// 	}
	// 	setCitas(citassArr[1]);
	// };
	// useEffect(() => {
	// 	getCitasPaciente();
	// }, []);

	const deleteCita = async () => {
		axios.delete(`${URI}deleteCita/${cita._id}`);

		const newCitasPac = citas.filter((cita) => cita == cita.fecha_cita);

		await axios.put(`${URI}updatePaciente/${_id_paciente}`, {
			citas: newCitasPac,
		});

		const newCitasMed = medico.disponible.filter((disp) => disp == cita.fecha_cita);
		const newDisp = medico.disponible;
		newDisp.push(cita.fecha_cita);

		await axios.put(`${URI}updateMedico/${medico._id}`, {
			disponible: newDisp,
			citas: newCitasMed,
		});

		navigate(0);
	};

	return (
		<Container>
			<Col className="mt-5">
				<h3>
					<center>A CONTINUACIÓN ENCUENTRA TODAS SUS CITAS ASIGNADAS</center>
				</h3>
				<br />
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Especialidad</th>
							<th>Profesional</th>
							<th>Dirección</th>
							<th>Fecha</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						{citas.map((cita) =>
							medicos.map((medico) =>
								cita._id_medico == medico._id ? (
									<tr key={cita._id}>
										<th>{medico.especialidad}</th>
										<th>{medico.nombre}</th>
										<th>{medico.direccion}</th>
										<th>{cita.fecha_cita}</th>
										<th>
											<Link
												onClick={() => {
													localStorage.setItem("cita_edit", JSON.stringify(cita));
													localStorage.setItem("especialidad", JSON.stringify(medico.especialidad));
												}}
												className="fa-solid fa-pen-to-square"
												to={"/home/edit"}></Link>{" "}
											<Link
												onClick={() => {
													handleShowModalEliminar();
													setCita(cita);
													setMedico(medico);
												}}
												className="fa-solid fa-trash-can txt-bg-danger"
												style={{ color: "#ff5353" }}></Link>
										</th>
									</tr>
								) : null
							)
						)}
					</tbody>
				</Table>
			</Col>
			<Modal show={modalState === "modal-eliminar"}>
				<div className="modal-header">
					<div className="modal-title h4">
						CONFIRME SI DESEA <b>CANCELAR</b> SU CITA DE <b>{medico.especialidad}</b> CON EL MEDICO{" "}
						<b>{medico.nombre}</b> PARA LA FECHA <b>{cita.fecha_cita}</b>
						<br />
					</div>
					<Button
						variant="button"
						className="btn-close"
						aria-label="Close"
						onClick={handleClose}
						title="Cerrar"></Button>
				</div>
				<Modal.Body>
					<Row>
						<Button key="eliminar" variant="dark mt-3 btn-lg" onClick={deleteCita}>
							CANCELAR CITA
						</Button>
					</Row>
					<Row>
						<Button
							variant="light mt-3 btn-lg"
							aria-label="Cancelar"
							onClick={handleClose}
							title="Cancelar">
							VOLVER
						</Button>
					</Row>
				</Modal.Body>
			</Modal>
		</Container>
	);
};

export default CompCitasListar;
