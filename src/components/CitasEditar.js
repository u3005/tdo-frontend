import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import moment from "moment";

const URI = require("./URI");

const CompCitasEditar = () => {
	const [modalState, setModalState] = useState("close");
	const handleShowModalAgendar = () => {
		setModalState("modal-agendar");
	};
	const handleClose = () => {
		setModalState("close");
	};

	const navigate = useNavigate();

	const [fecha_cita, setFechaCita] = useState("");
	const [fecha_solicitud, setFechaSolicitud] = useState("");
	var [_id_paciente, set_IdPaciente] = useState("");

	var [paciente, setPaciente] = useState("");

	const [medicos, setMedicos] = useState([]);
	var [medico, setMedico] = useState("");
	var [medicoSel, setMedicoSel] = useState("");
	var [especialidad, setEspecialidad] = useState("");

	var [cita_edit, setCitaEdit] = useState("");

	useEffect(() => {
		_id_paciente = JSON.parse(localStorage.getItem("_id_paciente"));
		set_IdPaciente(_id_paciente);
		especialidad = JSON.parse(localStorage.getItem("especialidad"));
		setEspecialidad(especialidad);
		cita_edit = JSON.parse(localStorage.getItem("cita_edit"));
		setCitaEdit(cita_edit);
	}, []);

	const getMedicos = async () => {
		const res = await axios.get(`${URI}getMedicos/`);
		const medicosArr = [];
		for (var i in res.data) {
			medicosArr.push(i, res.data[i]);
		}
		setMedicos(medicosArr[1]);
	};

	const getPacientes = async () => {
		const res = await axios.get(`${URI}getPacientes/${_id_paciente}`);
		const paciente = [];
		for (var i in res.data) {
			paciente.push(i, res.data[i]);
		}
		paciente[1].map((data) => setPaciente(data));
	};

	useEffect(() => {
		getPacientes();
		getMedicos();
	}, []);

	const getDisponibles = (esp) => {
		return (
			<>
				<thead>
					<tr>
						<th>MEDICO</th>
						<th>DIRECCIÓN</th>
						<th>CITAS DISPONIBLES</th>
					</tr>
				</thead>
				<tbody>
					{medicos.map((medico) =>
						medico.especialidad === esp ? (
							<tr key={medico._id}>
								<td>{medico.nombre}</td>
								<td>{medico.direccion}</td>
								<td>
									{medico.disponible.map((disponible, index) => (
										<Button
											variant="dark btn-sm mx-2 mb-1"
											onClick={(e) => {
												selCita(medico, disponible);
											}}
											key={index}>
											{disponible}
										</Button>
									))}
								</td>
							</tr>
						) : null
					)}
				</tbody>
			</>
		);
	};

	const selCita = (medicoSel, disponibleSel) => {
		handleShowModalAgendar();
		setFechaSolicitud(moment().format("YYYY/MM/DD, hh:mm"));
		setFechaCita(disponibleSel);
		setMedicoSel(medicoSel);
		medicos.map((medico) => (medico._id == cita_edit._id_medico ? setMedico(medico) : null));
	};

	const editCita = async (e) => {
		e.preventDefault();

		var newCitas = paciente.citas.filter((cita) => cita == cita_edit);
		newCitas.push(fecha_cita);

		await axios.put(`${URI}updatePaciente/${_id_paciente}`, {
			citas: newCitas,
		});

		newCitas = medico.citas.filter((cita) => cita == fecha_cita);
		var newDisp = medico.disponible;
		newDisp.push(cita_edit.fecha_cita);

		await axios.put(`${URI}updateMedico/${medico._id}`, {
			disponible: newDisp,
			citas: newCitas,
		});

		newCitas = medicoSel.citas;
		medicoSel.citas.push(fecha_cita);
		newDisp = medicoSel.disponible.filter((cita) => cita == fecha_cita);

		await axios.put(`${URI}updateMedico/${medicoSel._id}`, {
			disponible: newDisp,
			citas: newCitas,
		});

		await axios.put(`${URI}updateCita/${cita_edit._id}`, {
			fecha_cita: fecha_cita,
			fecha_solicitud: fecha_solicitud,
			_id_medico: medicoSel._id,
		});

		navigate("/home/list");
	};

	return (
		<>
			<Container>
				<Row className="mt-5">
					<Col>
						<Card className="text-center">
							<Card.Header>
								REAGENDAR CITA MÉDICA DE <b>{especialidad}</b>{" "}
							</Card.Header>
							<Card.Title className="mt-2 mx-2">SELECCIONE LA NUEVA CITA PARA REAGENDAR</Card.Title>
							<Card.Body>
								<Table striped bordered hover responsive>
									{getDisponibles(especialidad)}
								</Table>
							</Card.Body>
						</Card>
						<Modal show={modalState === "modal-agendar"}>
							<div className="modal-header">
								<div className="modal-title h4">
									CONFIRME SU REAGENDAMIENTO PARA <b>{especialidad}</b> CON
									<br />
									<b>{medicoSel.nombre}</b> EL DÍA <b>{fecha_cita}</b>
								</div>
								<Button
									variant="button"
									className="btn-close"
									aria-label="Close"
									onClick={handleClose}
									title="Cerrar"></Button>
							</div>
							<Modal.Body>
								SELECCIONE <b>REAGENDAR</b> SI DESEA AGENDAR LA CITA, DE LO CONTRARIO PULSE CANCELAR
								<br /> <br />
								<Row>
									<Button key="agendar" variant="dark mt-3 btn-lg" onClick={editCita}>
										REAGENDAR
									</Button>
								</Row>
								<Row>
									<Button
										variant="light mt-3 btn-lg"
										aria-label="Cancelar"
										onClick={handleClose}
										title="Cancelar">
										CANCELAR
									</Button>
								</Row>
							</Modal.Body>
						</Modal>
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default CompCitasEditar;
