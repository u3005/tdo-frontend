import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate, Outlet } from "react-router-dom";

const PrivateRoute = () => {
	// state
	const [loading, setLoading] = useState(true);

	const navigate = useNavigate();

	// check if user is logged in
	// by making API request or from localStorage
	useEffect(() => {
		const authCheck = async () => {
			const { data } = await axios(`/auth-check`);
			if (!data.ok) {
				setLoading(true);
			} else {
				setLoading(false);
			}
		};
		authCheck();
	}, []);

	return loading ? "" : <Outlet />;
};

export default PrivateRoute;
