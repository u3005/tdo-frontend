import { useState, useEffect } from "react";
import axios from "axios";
import baseURI from "./URI";

let URI = "";
const _id_paciente = JSON.parse(localStorage.getItem("_id_paciente"));
// var [arr, setArr] = useState([]);

function setPath(path) {
	switch (path) {
		case "CitasPaciente":
			URI = `${baseURI}get${path}/${_id_paciente}`;
			break;
		case "Medicos":
			URI = `${baseURI}get${path}`;
			break;
	}
	return URI;
}

async function getRes(path) {
	const res = await axios.get(URI);
	var arr = [];
	for (var i in res.data) {
		arr.push(i, res.data[i]);
	}
	var arrRes = arr[1];
	// console.log("arrRes", arrRes);
	return arrRes; // Devuelve arrays de Objects
}

export function GetFromDB(path) {
	// var arr = new Map();
	// var arr = [];
	// paths.map((path) => {
	// 	getRes(setPath(path)).then((res) => arr.set(path, res));
	// });

	// getRes(setPath(path))
	// 	.then((res) => {
	// 		// console.log("res: ", res);
	// 		var arr = res;
	// 		return arr;
	// 	})
	// 	.catch((err) => console.log("err: ", err));

	var arr = [];
	arr = getRes(setPath(path));
	return arr;
	// console.log("all arrrr", arr);
	// arr.set("aAlgp", "alcAo");
}

// export function GetFrompath() {
// 	return console.log("worrkin");
// }
