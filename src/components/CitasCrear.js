import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import moment from "moment";

const URI = require("./URI");

const CompCitasCrear = () => {
	const [modalState, setModalState] = useState("close");
	const handleShowModalMedico = () => {
		setModalState("modal-medico");
	};
	const handleShowModalAgendar = () => {
		setModalState("modal-agendar");
	};
	const handleClose = () => {
		setModalState("close");
	};

	const navigate = useNavigate();

	const [fecha_cita, setFechaCita] = useState("");
	const [fecha_solicitud, setFechaSolicitud] = useState("");
	var [_id_paciente, set_IdPaciente] = useState("");
	const [_id_medico, set_IdMedico] = useState("");

	const [medico, setMedico] = useState("");
	const [medicos, setMedicos] = useState([]);
	const [especialidad, setEspecialidad] = useState("");
	const [especialidades, setEspecialidades] = useState([]);
	const [cita, setCita] = useState("");

	useEffect(() => {
		_id_paciente = JSON.parse(localStorage.getItem("_id_paciente"));
		set_IdPaciente(_id_paciente);
	});

	const getMedicos = async () => {
		const res = await axios.get(`${URI}getMedicos/`);
		const medicosArr = [];
		for (var i in res.data) {
			medicosArr.push(i, res.data[i]);
		}
		setMedicos(medicosArr[1]);
		setEspecialidades([...new Set(medicosArr[1].map(({ especialidad }) => especialidad))]);
	};

	useEffect(() => {
		getMedicos();
	}, []);

	const getDisponibles = (esp) => {
		for (let i of especialidades) {
			if (i === esp) {
				return (
					<>
						<thead>
							<tr>
								<th>MEDICO</th>
								<th>DIRECCIÓN</th>
								<th>CITAS DISPONIBLES</th>
							</tr>
						</thead>
						<tbody>
							{medicos.map((medico) =>
								medico.especialidad === esp ? (
									<tr key={medico._id}>
										<td>{medico.nombre}</td>
										<td>{medico.direccion}</td>
										<td>
											{medico.disponible.map((disponible, index) => (
												<Button
													variant="dark btn-sm mx-2 mb-1"
													onClick={(e) => {
														selCita(medico, disponible);
													}}
													key={index}>
													{disponible}
												</Button>
											))}
										</td>
									</tr>
								) : null
							)}
						</tbody>
					</>
				);
			}
		}
	};

	const selCita = (medico, disponible) => {
		handleShowModalAgendar();
		setCita(medico.nombre + ": " + disponible);
		setFechaSolicitud(moment().format("YYYY/MM/DD, hh:mm"));
		setFechaCita(disponible);
		setMedico(medico);
		set_IdMedico(medico._id);
	};

	const createCita = async (e) => {
		e.preventDefault();

		await axios.post(`${URI}createCita/`, {
			fecha_cita: fecha_cita,
			fecha_solicitud: fecha_solicitud,
			_id_paciente: _id_paciente,
			_id_medico: _id_medico,
		});

		var newDisponible = [];
		var newCitas = [];

		medico.disponible.map((disponible) =>
			disponible !== fecha_cita ? newDisponible.push(disponible) : newCitas.push(fecha_cita)
		);

		await axios.put(`${URI}updateMedico/${medico._id}`, {
			disponible: newDisponible,
			citas: newCitas,
		});

		await axios.put(`${URI}updatePaciente/${_id_paciente}`, {
			citas: newCitas,
		});

		navigate("/home/list");
	};

	return (
		<>
			<Container style={{ width: "25rem" }}>
				<Row className="mt-5">
					<Col>
						<Card className="text-center">
							<Card.Header>AGENDAMIENTO DE CITA MÉDICA</Card.Header>
							<Card.Title className="mt-2 mx-2">
								SELECCIONE LA ESPECIALIDAD QUE DESEA AGENDAR
							</Card.Title>
							<Card.Body>
								<Row className="mx-2">
									{especialidades.map((especialidad) => (
										<Button
											key={especialidad}
											variant="dark mt-2"
											onClick={() => {
												handleShowModalMedico();
												setEspecialidad(especialidad);
											}}>
											{especialidad}
										</Button>
									))}
								</Row>
							</Card.Body>
						</Card>
						<Modal size="lg" show={modalState === "modal-medico"}>
							<div className="modal-header">
								<div className="modal-title h4">MEDICOS PARA {especialidad}</div>
								<Button
									variant="button"
									className="btn-close"
									aria-label="Close"
									onClick={handleClose}
									title="Cerrar"></Button>
							</div>
							<Modal.Body>
								<form onSubmit={createCita}>
									SELECCIONE LA CITA QUE DESEA AGENDAR <br /> <br />
									<Table striped bordered hover responsive>
										{getDisponibles(especialidad)}
									</Table>
								</form>
							</Modal.Body>
						</Modal>
						<Modal show={modalState === "modal-agendar"}>
							<div className="modal-header">
								<div className="modal-title h4">
									CONFIRME SU AGENDAMIENTO PARA <b>{especialidad}</b> CON
									<br />
									<b>{cita}</b>
								</div>
								<Button
									variant="button"
									className="btn-close"
									aria-label="Close"
									onClick={handleClose}
									title="Cerrar"></Button>
							</div>
							<Modal.Body>
								SELECCIONE <b>AGENDAR</b> SI DESEA AGENDAR LA CITA, DE LO CONTRARIO PULSE CANCELAR
								<br /> <br />
								<Row>
									<Button key="agendar" variant="dark mt-3 btn-lg" onClick={createCita}>
										AGENDAR
									</Button>
								</Row>
								<Row>
									<Button
										variant="light mt-3 btn-lg"
										aria-label="Cancelar"
										onClick={handleClose}
										title="Cancelar">
										CANCELAR
									</Button>
								</Row>
							</Modal.Body>
						</Modal>
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default CompCitasCrear;
