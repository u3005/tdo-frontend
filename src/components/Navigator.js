import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useLocation } from "react-router-dom";

function CompNavbar() {
	const noNavbar = ["/"];
	const { pathname } = useLocation();
	var [nombre, setNombre] = useState("");
	var [_id_paciente, set_IdPaciente] = useState("");

	useEffect(() => {
		nombre = JSON.parse(localStorage.getItem("nombre"));
		_id_paciente = JSON.parse(localStorage.getItem("_id_paciente"));
		setNombre(nombre);
		set_IdPaciente(_id_paciente);
	});

	const clearUser = () => {
		setNombre("");
		set_IdPaciente("");
	};

	if (noNavbar.some((item) => pathname === item)) return null;

	return (
		<Navbar bg="light" expand="lg">
			<Container className="container-fluid">
				<Navbar.Brand href="/home">TuDoctorOnline</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse
					id="basic-navbar-nav"
					className="justify-content-end"
					style={{ width: "100%" }}>
					<Nav.Item>{nombre}</Nav.Item>
					<Nav.Item className="text-muted">
						<a className="nav-link" href="/" onClick={clearUser}>
							Salir
						</a>
					</Nav.Item>
					{/*TODO logout y delete localstorage*/}
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}

export default CompNavbar;
