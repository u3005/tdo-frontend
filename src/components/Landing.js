import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const URI = require("./URI");

const CompLanding = () => {
	const [modalState, setModalState] = useState("close");
	const handleShowModalRegistro = () => {
		setModalState("modal-registro");
	};
	const handleShowModalLogin = () => {
		setModalState("modal-login");
	};
	const handleClose = () => {
		setModalState("close");
	};

	const [pacientes, setPacientes] = useState([]);
	const [_id_paciente, set_IdPaciente] = useState("");
	const [nombre, setNombre] = useState("");
	const [numero_id, setNumero_id] = useState("");
	const [ciudad, setCiudad] = useState("");
	const [direccion, setDireccion] = useState("");
	const [telefono, setTelefono] = useState("");
	const [email, setEmail] = useState("");

	const navigate = useNavigate();

	const loginUser = () => {
		const getPacientes = async () => {
			const pacientesArr = [];
			const res = await axios.get(`${URI}getPacientes/`);
			for (var i in res.data) {
				pacientesArr.push(i, res.data[i]);
			}
			setPacientes(pacientesArr[1]);
		};

		getPacientes();
		// eslint-disable-next-line no-lone-blocks
		{
			pacientes.map((paciente) =>
				paciente.numero_id == numero_id && paciente.email == email
					? (setNombre(paciente.nombre),
					  set_IdPaciente(paciente._id),
					  localStorage.setItem("nombre", JSON.stringify(paciente.nombre)),
					  localStorage.setItem("_id_paciente", JSON.stringify(paciente._id)),
					  navigate("/home"))
					: null
			);
		}
		//TODO como validar de forma segura un usuario existente?
	};

	useEffect(() => {
		localStorage.setItem("nombre", JSON.stringify(nombre));
		localStorage.setItem("_id_paciente", JSON.stringify(_id_paciente));
	});

	const createUser = async (e) => {
		e.preventDefault();
		await axios
			.post(`${URI}createPaciente/`, {
				nombre: nombre,
				numero_id: numero_id,
				ciudad: ciudad,
				direccion: direccion,
				telefono: telefono,
				email: email,
			})
			.then(function (response) {
				set_IdPaciente(JSON.stringify(response.data.message._id).replaceAll(`"`, ``));
			});

		navigate("/home");
	};

	useEffect(() => {
		localStorage.setItem("nombre", JSON.stringify(nombre));
		localStorage.setItem("_id_paciente", JSON.stringify(_id_paciente));
	});

	return (
		<>
			<Navbar bg="light" expand="lg">
				<Container>
					<Navbar.Brand href="">TuDoctorOnline</Navbar.Brand>
				</Container>
			</Navbar>
			<Container style={{ width: "25rem" }}>
				<Col className="mt-5">
					<Card className="text-center">
						<Card.Header>
							AQUI PODRÁ AGENDAR Y CONSULTAR SUS SERVICIOS MÉDICOS DE MANERA FÁCIL Y RÁPIDA
						</Card.Header>
						<Card.Body>
							<Row>
								<Col>
									<Button variant="dark" onClick={handleShowModalRegistro}>
										QUIERO REGISTRARME
									</Button>
								</Col>
								<Col>
									<Button variant="dark" onClick={handleShowModalLogin}>
										YA ESTOY REGISTRADO(A)
									</Button>
								</Col>
							</Row>
						</Card.Body>
					</Card>
				</Col>
			</Container>
			<Modal show={modalState === "modal-registro"}>
				<div className="modal-header">
					<div className="modal-title h4">USUARIO NUEVO</div>
					<button
						type="button"
						className="btn-close"
						aria-label="Close"
						onClick={handleClose}
						title="Cerrar"></button>
				</div>
				<Modal.Body>
					INGRESE A CONTINUACIÓN SUS DATOS PERSONALES <br /> <br />
					<form onSubmit={createUser}>
						<InputGroup size="sm" className="mb-2" required hasValidation>
							<InputGroup.Text id="inputGroup-nombre">Nombre(s)</InputGroup.Text>
							<Form.Control
								aria-label="nombres"
								type="text"
								value={nombre}
								onChange={(e) => setNombre(e.target.value)}
								required
							/>
						</InputGroup>
						<br />
						<InputGroup size="sm" className="mb-2">
							<InputGroup.Text id="inputGroup-documento">No. Identificación</InputGroup.Text>
							<Form.Control
								aria-label="documento de identidad"
								type="number"
								value={numero_id}
								onChange={(e) => setNumero_id(e.target.value)}
								required
							/>
						</InputGroup>
						<br />
						<InputGroup size="sm" className="mb-2">
							<InputGroup.Text id="inputGroup-ciudad">Ciudad</InputGroup.Text>
							<Form.Control
								aria-label="ciudad"
								type="text"
								value={ciudad}
								onChange={(e) => setCiudad(e.target.value)}
								required
							/>
						</InputGroup>{" "}
						<br />
						<InputGroup size="sm" className="mb-2">
							<InputGroup.Text id="inputGroup-direccion">Dirección</InputGroup.Text>
							<Form.Control
								aria-label="dirección"
								type="text"
								value={direccion}
								onChange={(e) => setDireccion(e.target.value)}
								required
							/>
						</InputGroup>
						<br />
						<InputGroup size="sm" className="mb-2">
							<InputGroup.Text id="inputGroup-telefono">Teléfono</InputGroup.Text>
							<Form.Control
								aria-label="telefono"
								type="number"
								value={telefono}
								onChange={(e) => setTelefono(e.target.value)}
								required
							/>
						</InputGroup>
						<br />
						<InputGroup size="sm" className="mb-2">
							<InputGroup.Text id="inputGroup-email">E-mail</InputGroup.Text>
							<Form.Control
								aria-label="email"
								type="email"
								value={email}
								onChange={(e) => setEmail(e.target.value)}
								required
							/>
						</InputGroup>
						<br />
						<Button variant="dark" type="submit">
							CREAR USUARIO
						</Button>
					</form>
				</Modal.Body>
			</Modal>
			<Modal show={modalState === "modal-login"}>
				<div className="modal-header">
					<div className="modal-title h4">USUARIO EXISTENTE</div>
					<button
						type="button"
						className="btn-close"
						aria-label="Close"
						onClick={handleClose}
						title="Cerrar"></button>
				</div>
				<Modal.Body>
					<form>
						INGRESE A CONTINUACIÓN SUS DATOS PERSONALES <br /> <br />
						<InputGroup size="sm" className="mb-2" required hasValidation>
							<InputGroup.Text id="inputGroup-documento">No. Identificación</InputGroup.Text>
							<Form.Control
								aria-label="documento de identidad"
								type="number"
								value={numero_id}
								onChange={(e) => setNumero_id(e.target.value)}
								required
							/>
						</InputGroup>
						<br />
						<InputGroup size="sm" className="mb-2">
							<InputGroup.Text id="inputGroup-email">E-mail</InputGroup.Text>
							<Form.Control
								aria-label="email"
								type="email"
								value={email}
								onChange={(e) => setEmail(e.target.value)}
								required
							/>
						</InputGroup>
						<br />
						<Button
							variant="dark"
							onClick={(e) => {
								loginUser();
							}}>
							VALIDAR USUARIO
						</Button>
					</form>
				</Modal.Body>
			</Modal>
		</>
	);
};

export default CompLanding;
